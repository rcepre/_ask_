import random
import string

ALPHANUMERIC_CHARS = string.ascii_lowercase + string.digits
STRING_LENGHT = 6


def gen_random_str(chars=ALPHANUMERIC_CHARS, lenght=STRING_LENGHT):
    """ Return a 6 chars lenght randomised string
        -> ksyvks, owjfos, oehfos, idofns
    """
    return "".join(random.choice(chars) for _ in range(lenght))
