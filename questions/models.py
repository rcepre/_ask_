from django.conf import settings
from django.db import models


class Question(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE,
                               related_name="questions")
    content = models.CharField(max_length=512)
    slug = models.SlugField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # SETTINGS
    # Threshold from which is considered as trending, according to the number of answers in the last week.
    TRENDING_THRESHOLD = 2

    def __str__(self):
        return self.content


class Answer(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    body = models.TextField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE,
                                 related_name='answers')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    voters = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                    related_name='votes')

    def __str__(self):
        return self.author.username
