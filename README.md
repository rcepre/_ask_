# Ask

![purpose](https://img.shields.io/badge/purpose-education-green.svg)
![purpose](https://img.shields.io/badge/udemy-red.svg)


Result of the online course *"The Complete Guide to Django REST Framework and Vue JS"* from Udemy



This project is an introduction to the Django Rest Framework and VueJS.
It's a simple reproduction of Quora: An user can ask a question, others can answer it. 
A simple like / dislike system is implemented.
The authentication system is provided by the `django rest auth` and the `django allauth` packages.
The project is made with an API-First approch.

Technologies:


- Ptyhon Django
- Django Rest Framework
- Django Rest Auth
- Django Allauth
- Django Webpack Loader
- Webpack
- Babel
- VueJS
- Bootstrap