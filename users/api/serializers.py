from rest_framework import serializers

from users.models import Person


class PersonDisplaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ['username']
