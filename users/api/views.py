from rest_framework.response import Response
from rest_framework.views import APIView

from users.api.serializers import PersonDisplaySerializer


class CurrentPersonAPIView(APIView):

    def get(self, request):
        serializer = PersonDisplaySerializer(request.user)
        return Response(serializer.data)
