from django.urls import path

from users.api.views import CurrentPersonAPIView

urlpatterns = [
    path("user/", CurrentPersonAPIView.as_view(), name="current-user")
]