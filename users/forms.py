from django_registration.forms import RegistrationForm

from users.models import Person


class PersonForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = Person
