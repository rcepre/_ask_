from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from users.models import Person


class CustomUserAdmin(UserAdmin):
    model = Person
    list_display = ['username', 'email', 'is_staff']


admin.site.register(Person, CustomUserAdmin)
